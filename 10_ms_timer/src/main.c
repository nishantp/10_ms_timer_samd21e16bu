/**
 * \file
 *
 * \brief SAM TC - Timer Counter Driver Quick Start
 *
 * Copyright (C) 2014-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <asf.h>
//#include "conf_qs_tc_timer.h"

#define LED1_PIN                  PIN_PA00
#define LED1_ACTIVE               false
#define LED1_INACTIVE             !LED1_ACTIVE

#define LED_1_NAME                "LED1 (yellow)"
#define LED_1_PIN                 LED1_PIN
#define LED_1_ACTIVE              LED1_ACTIVE
#define LED_1_INACTIVE            LED1_INACTIVE
#define LED1_GPIO                 LED1_PIN
#define LED1                      LED1_PIN


#define LED2_PIN                  PIN_PA01
#define LED2_ACTIVE               false
#define LED2_INACTIVE             !LED2_ACTIVE

#define LED_2_NAME                "LED2 (yellow)"
#define LED_2_PIN                 LED2_PIN
#define LED_2_ACTIVE              LED2_ACTIVE
#define LED_2_INACTIVE            LED2_INACTIVE
#define LED2_GPIO                 LED2_PIN
#define LED2                      LED2_PIN

void configure_tc(int);
void configure_tc_callbacks(void);
void tc_callback_to_toggle_led(
		struct tc_module *const module_inst);

volatile uint8_t count=0;
//! [module_inst]
struct tc_module tc_instance;
//! [module_inst]

//! [callback_funcs]
void tc_callback_to_toggle_led(
		struct tc_module *const module_inst)
{
	
	count++;
	if(count==100)
	{
	port_pin_toggle_output_level(LED_1_PIN);
	port_pin_toggle_output_level(LED_2_PIN);
	count=0;
	}
	
}
//! [callback_funcs]

//! [setup]
void configure_tc(int period)
{
	//! [setup_config]
	struct tc_config config_tc;
	//! [setup_config]
	//! [setup_config_defaults]
	tc_get_config_defaults(&config_tc);
	//! [setup_config_defaults]

	//! [setup_change_config]
	config_tc.counter_size = TC_COUNTER_SIZE_16BIT;
	config_tc.clock_source = GCLK_GENERATOR_1;
	config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV1;
	//config_tc.counter_16_bit.period = 100;
	config_tc.reload_action=TC_RELOAD_ACTION_RESYNC;
	config_tc.wave_generation=TC_WAVE_GENERATION_MATCH_PWM;
	config_tc.counter_16_bit.compare_capture_channel[0] = period;
	//config_tc.counter_8_bit.compare_capture_channel[1] = 54;
	//! [setup_change_config]

	//! [setup_set_config]
	tc_init(&tc_instance, TC4, &config_tc);
	//! [setup_set_config]

	//! [setup_enable]
	tc_enable(&tc_instance);
	//! [setup_enable]
}

void configure_tc_callbacks(void)
{
	//! [setup_register_callback]
	//tc_register_callback(&tc_instance, tc_callback_to_toggle_led,TC_CALLBACK_OVERFLOW);
	tc_register_callback(&tc_instance, tc_callback_to_toggle_led,TC_CALLBACK_CC_CHANNEL0);
	//tc_register_callback(&tc_instance, tc_callback_to_toggle_led,TC_CALLBACK_CC_CHANNEL1);
	//! [setup_register_callback]

	//! [setup_enable_callback]
	//tc_enable_callback(&tc_instance, TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&tc_instance, TC_CALLBACK_CC_CHANNEL0);
	//tc_enable_callback(&tc_instance, TC_CALLBACK_CC_CHANNEL1);
	//! [setup_enable_callback]
}
//! [setup]



int main(void)
{
	struct port_config pin_conf;
	port_get_config_defaults(&pin_conf);

	/* Configure LEDs as outputs, turn them off */
	pin_conf.direction  = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(LED_1_PIN, &pin_conf);
	port_pin_set_output_level(LED_1_PIN, LED_1_INACTIVE);
	port_pin_set_config(LED_2_PIN, &pin_conf);
	port_pin_set_output_level(LED_2_PIN, LED_2_INACTIVE);
	system_init();
	
//! [setup_init]
	configure_tc(327);      //1S=32768  10ms=327.68
	configure_tc_callbacks();
//! [setup_init]

//! [main]
	//! [enable_global_interrupts]
	system_interrupt_enable_global();
	//! [enable_global_interrupts]

	//! [main_loop]
	while (true) {
	}
	//! [main_loop]
//! [main]
}
